<?php include("top.html"); ?>
  <div id="index">
    <div id="map"></div>

    <h2>Business hours: 10:00am to 11:00pm, Monday through Saturday.</h2>
    <h2>206.555.1212</h2>
    <address>4306 University Way NE <br/> Seattle, WA 98105</address>
    <h2><a href="mailto:dawgpizzaseattle@gmail.com">Email Us</a></h2>
    <h2><a href="https://twitter.com/DawgPizzaSea">@DawgPizzaSea</a></h2>
  <script type="text/javascript">
         window.onload = initializeMap();
           function initializeMap() {
                var latlng = new google.maps.LatLng(47.660048,-122.312971);
                var settings = {
                  zoom: 19,
                  center: latlng,
                  mapTypeControl: true,
                  mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                  navigationControl: true,
                  navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                  mapTypeId: google.maps.MapTypeId.SATELLITE};
                var map = new google.maps.Map(document.getElementById("map"), settings);
                var companyMarker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    title: "Dawg Pizza"});
            }                        
  </script>
  <?php include("bottom.html"); ?>
