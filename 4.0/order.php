<?php include("top.html"); ?>
      <div id="index">
    <h2 id="adj">All our pizzas are made with hand-thrown thin crust, baked in real wood ovens.
        Choose from one of these sizes:<br/>
       Small (12"), Medium (14"), Large (17")
    </h2>
        <h3><strong>FREE Delivery! All Orders must be at least $20</strong></h3>
    <h3>*We deliver within the Seattle City limits, From noon to 11:00pm, and we accept cash or credit cards on delivery.</h3>
      <div id="order_form">
      <h2> First Step: We will need your address: </h2>
        <input type="text" value="Name" id="customer"/>
        <input type="text" value="Address" id="addr"/>
        <input type="text" value="Address Line Two" id="addr2"/>
        <input type="text" value="Zip" id="zip"/>
        <input type="text" value="Phone" id="phone"/>
         <ul id="min_car">
        <li> Your Cart: </li>
         <li><small  id="total"></small> </li>
         <li><small id="tax"></small> </li>
          <li><small id="sum_total"></small> </li>
         
          <li> <small id="reset">RESET ORDER </small> </li>
      </ul>
      </div>
      <h2 id="order_food">Second Step: Click for Menu! </h2>
    <div itemprop="menu" id="options"> 
      <ul id="meat">
          <li class="name"><h2>Meat Pies</h2></li>
      </ul>

      <ul id="veg">
           <li class="name"><h2>Vegetarian Pies</h2></li> 
      </ul>

      <div id="misce">
      <ul id="bev">
        <li class="name"><h2>Drinks</h2></li>
      </ul>
      <ul id="dessert">
        <li class="name"><h2>Dessert</h2></li>
      </ul>
      </div>
         <form name="input" action="http://dawgpizza.com/orders/" method="POST">
            <input id="send" type="hidden" name="cart">
            <input id="checked" type="submit" value="Order!">
         </form>
    </div><!-- Menu / Options -->
     <script src="pizza_files/menu.js" type="text/javascript"></script>
     <script src="pizza_files/site.js" type="text/javascript"></script>
      <script src="pizza_files/order.js" type="text/javascript"></script>
    <?php include("bottom.html"); ?>
