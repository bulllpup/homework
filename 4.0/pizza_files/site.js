//Connor Coleman
//Info 343 HW #3
//
//Performs Dynamic Menu Updating from JS object;
//Menu Object can be seen in 'MENU.JS'
//This space is used for redundant JS function

window.onLoad = render();

//populate page with relevant menu content.
function render() {
    pizza();
    misc('bev', com.dawgpizza.menu.drinks);
    misc('dessert', com.dawgpizza.menu.desserts);
}

//update pizza objects to menu page;
function pizza() {
    for (var i = 0; i < com.dawgpizza.menu.pizzas.length; i++) {
        //http://jsperf.com/html-insert-vs-createelement -> for results
        var pizza = com.dawgpizza.menu.pizzas[i];
        var name = '<li class="name"><strong>' + pizza.name + '</strong></li>';
        name += '<li>' + pizza.description;
        name += '<select id="' + i + '">';
        var size = ["Small", "Medium", "Large"]
        for (var j = 0; j < pizza.prices.length; j++) {
            name +=  '<option class="' +  pizza.prices[j] + '">$' + pizza.prices[j] + " (" +size[j] + ')</option>';
        }
        name += '</select></li>';
        if (pizza.vegetarian) {
            document.getElementById('veg').innerHTML += name;
        }
        else {
            document.getElementById('meat').innerHTML += name;
        }
    }
}
//update misc. beverages / desserts to page;
function misc(to, obj) {
    var ins = "";
    for (var i = 0; i < obj.length; i++) {
        ins += '<li>';
        var item = obj[i];
        ins += item.name + '<button type="button">$' + item.price + '</button> </li>';
    }
    document.getElementById(to).innerHTML += ins;
}