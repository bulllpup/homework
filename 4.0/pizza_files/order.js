//Connor Coleman, Info 343, V4.0
//Allows a user to purchase a pizza/dessert/drink online,
//and have it ordered to their provided address.
//doesn't allow user to see form until address is filled out.

var cart; //shopping cart;
var total; //total amount thus far, just for user's viewing
//real total is calculated server-side.
window.onLoad = render();

//initialize cart object, attach event handlers,
//and populate page (if cookie is available).
function render() {
    cart = {};
    total = 0;
    cart.items = [];
    //doesn't allow user to see form until address is filled out.
    document.getElementById('options').style.display = "none";
    document.getElementById('order_food').addEventListener('click', setAddress, false);
    cookieExist();
    eventHandler();
}

//attaches basic events for ordering through menu.
function eventHandler() {
    var parent = document.getElementsByTagName('select');
    for (var i = 0; i < parent.length; i++) {
        parent[i].addEventListener('click', addPizza, false);
    }
    var parent1 = document.getElementsByTagName('button');
    for (var i = 0; i < parent1.length; i++) {
        parent1[i].addEventListener('click', addMisc, false);
    }
}

//removes the user-indicated item from the cart;
//pre: accepts event of last item 'e'.
function remove(e) {
    var get = this.parentNode.innerHTML.split('-');
    var name = get[0];
    //Uses delimiters to parse text, and then splits it to array;
    var price = Number(get[1].split('<')[0].replace('(', '').replace(')', '').replace('$',''));
    total -= price;
    alert("Item was deleted");
    this.parentNode.remove();
    //removes item from cart in quickest manner;
    for (var i = cart.items.length - 1; i >= 0; i--) {
        if (cart.items[i].name == name &&
        cart.items[i].price == price) {
            cart.items.splice(i, i + 1);
        }
    }
    updateTotal();
}

//adds a pizza to cart;
//pre: accepts event of last item 'e'.
function addPizza(e) {
    var clickedItem = e.target.id;
    var pric = Number(document.getElementById(clickedItem).value.substr(1, 3));
    var size = document.getElementById(clickedItem).value.substr(4).replace('(', '').replace(')', '').toLowerCase();
    var name = document.getElementById(clickedItem).parentNode.previousSibling.childNodes[0].innerHTML;
    total += pric;
    var pizza = {};
    pizza.price = pric;
    pizza.type = "pizza";
    pizza.name = name;
    pizza.size =  size;
    cart.items.push(pizza);
    addToCart(name, pric);
    updateTotal();
    alert(name + " added, Thank you!");
}

//adds a dessert/drink to the cart;
//pre: accepts event of last item 'e'.
function addMisc(e) {
    var pric = Number(this.innerHTML.substr(1));
    total += pric;
    var misc = {};
    misc.price = pric;
    if (this.parentNode.parentNode.id == "dessert") {
        misc.type = "dessert";
    } else {
        misc.type = "drink";
    }
    misc.name = this.parentNode.innerHTML.split("<")[0];
    cart.items.push(misc);
    addToCart(misc.name, pric);
    updateTotal();
    alert(misc.name + " added, Thank you!");
}

//updates purchase total at top of shopping cart + tax.
function updateTotal() {
    document.getElementById('total').innerHTML = 'Total: $' + total;
    document.getElementById('tax').innerHTML = "+$" + tax() + ' (9.5% tax)';
    document.getElementById('sum_total').innerHTML = 'Total + Tax: $' + taxTotal();
    document.getElementById('send').value = JSON.stringify(cart);
    var parent = document.getElementsByClassName('delete');
    for (var i = 0; i < parent.length; i++) {
        parent[i].addEventListener('click', remove, false);
    }
    //attach event listeners to remove objects (if necessary).
}

//clears current cart;
//keeps user's address.
function changePage(e) {
    cart.items = [];
    total = 0;
    document.getElementById('total').innerHTML = '';
    document.getElementById('tax').innerHTML = '';
    document.getElementById('sum_total').innerHTML = '';
    document.getElementById('send').value = JSON.stringify(cart);
    removeR(document.getElementsByClassName('delete'));
}

//remove (Recursively) all parent elements matching given pattern;
function removeR(child) {
    if (child[0]) { //this is a little wierd, but it works surprisingly well!
        child[0].parentNode.remove();
        removeR(child);
    }
}

//sets user's address to specified cart.
//if a cookie exists, it populates fields based on that.
function setAddress(e) {
	cart.nextUrl = "http://students.washington.edu/connorco/info343/HW/4.0/order.php";
	cart.nextCaption = "Submit your order";
	cart.address2 = document.getElementById('addr2').value;
	if (checkFields()) {
	    saveCookie();
	    document.getElementById('options').style.display = "inline-block";
	}
	else {
	    alert("You might have forgot to enter your address?");
	}
	document.getElementById('reset').addEventListener('click', changePage, false);
}

//checks fields to make sure there is no basic user error;
//could add regex / pattern checking to improve.
//post: returns true if nothing is wrong with fields, false otherwise.
function checkFields() {
    var check = true;
    if (document.getElementById('customer').value != "Name" &&
    document.getElementById('customer').value != "") {
        cart.name = document.getElementById('customer').value;
    } else {
        document.getElementById('customer').style.color = "red";
        check = false;
    }
    if (document.getElementById('addr').value != "Address" &&
    document.getElementById('addr').value != "") {
        cart.address1 = document.getElementById('addr').value;
    } else {
        document.getElementById('addr').style.color = "red";
        check = false;
    }
    if (document.getElementById('zip').value != "Zip" &&
    document.getElementById('zip').value != "") {
        cart.zip = document.getElementById('zip').value;
    } else {
        document.getElementById('zip').style.color = "red";
        check = false;
    }
    if (document.getElementById('phone').value != "Phone" &&
    document.getElementById('phone').value != "") {
        cart.phone = document.getElementById('phone').value;
    } else {
        document.getElementById('phone').style.color = "red";
        check = false;
    }
    return check;
}

//pre: accepts a name and price of an item;
//adds an item to the cart picture-metaphor.
function addToCart(name, price) {
    var ins = '<li>' + name + "-($" + price + ')<span class="delete"> DELETE</span></li>';
    document.getElementById('min_car').innerHTML += ins;
    document.getElementById('reset').addEventListener('click', changePage, false);
}

//checks to see if a cookie currently exists;
//if cookie exists, it populates fields from cookie.
function cookieExist() {
    var check = findCookie();
    if (check != -1) {
        cart = JSON.parse(findCookie().substr(3));
        updateFromCookie();
    }
}

//populates field values with values from previous use.
function updateFromCookie() {
    if (cart.name) {
        document.getElementById('customer').value = cart.name;
        document.getElementById('addr').value = cart.address1;
        if (cart.address2) {
            document.getElementById('addr2').value = cart.address2;
        }
        document.getElementById('zip').value = cart.zip;
        document.getElementById('phone').value = cart.phone;
    }
}

//saves user's address for future easy usage.
function saveCookie() {
    var x = confirm("Would you like to save your address for future use?");
    if (x) {
        document.cookie= "DP:" + JSON.stringify(cart);
    }
}

//finds cookie that pertains to this form.
//post: returns cookie if is found, -1 if not.
function findCookie() {
    var cookies = document.cookie.split('; ');
    for (var i = 0; i < cookies.length; i++) {
        if (cookies[i].charAt(0) == "D" &&
            cookies[i].charAt(1) == "P" &&
        	cookies[i].charAt(2) == ":") {
        return cookies[i];
    	}
    }
    return -1;
}

//post: returns total + tax.
function taxTotal() {
    return total * 1.095;
}

//post: returns Washington Tax on items (9.5%).
function tax() {
    return total * 0.095;
}