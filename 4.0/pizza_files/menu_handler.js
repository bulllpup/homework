window.onLoad = eventHandler();
//handles events within MENU.
function eventHandler() {
    document.getElementById('options').style.display = "inline-block";
    document.getElementById('bev').style.opacity = ".00000009";
    document.getElementById('dessert').style.opacity = ".00000009";
    document.getElementById('meat').style.opacity = "1.0";
    document.getElementById('veg').style.opacity = "1.0";
    document.getElementById('piz').onclick = function () {
        document.getElementById('bev').style.opacity = ".00000009";
        document.getElementById('dessert').style.opacity = ".00000009";
        document.getElementById('meat').style.opacity = "1.0";
        document.getElementById('veg').style.opacity = "1.0";
    };
    document.getElementById('drk').onclick = function () {
        document.getElementById('meat').style.opacity = ".00000009";
        document.getElementById('veg').style.opacity = ".00000009";
        document.getElementById('dessert').style.opacity = ".00000009";
        document.getElementById('bev').style.opacity = "1.0";
        
    };
    document.getElementById('des').onclick = function () {
        document.getElementById('meat').style.opacity = ".00000009";
        document.getElementById('veg').style.opacity = ".00000009";
        document.getElementById('dessert').style.opacity = "1.0";
        document.getElementById('bev').style.opacity = ".00000009";
    };
}