//Connor Coleman
//Info 343 HW #3
//
//Performs Dynamic Menu Updating from JS object;
//Menu Object can be seen in 'MENU.JS'.
//I tried to use as simple/Efficient JS as I could,
//No external libraries are linked currently.
window.onLoad = render();

//populate page with relevant menu content.
function render() {
	pizza();
	misc('bev', com.dawgpizza.menu.drinks);
	misc('dessert', com.dawgpizza.menu.desserts);
	eventHandler();
}

//handles events within MENU.
function eventHandler() {
	document.getElementById('bev').style.opacity = ".00000009";
	document.getElementById('dessert').style.opacity = ".00000009";
	document.getElementById('meat').style.opacity = "1.0";
	document.getElementById('veg').style.opacity = "1.0";
	document.getElementById('piz').onclick = function () {
		document.getElementById('bev').style.opacity = ".00000009";
		document.getElementById('dessert').style.opacity = ".00000009";
		document.getElementById('meat').style.opacity = "1.0";
		document.getElementById('veg').style.opacity = "1.0";
	};
	document.getElementById('drk').onclick = function () {
		document.getElementById('meat').style.opacity = ".00000009";
		document.getElementById('veg').style.opacity = ".00000009";
		document.getElementById('dessert').style.opacity = ".00000009";
		document.getElementById('bev').style.opacity = "1.0";

	};
	document.getElementById('des').onclick = function () {
		document.getElementById('meat').style.opacity = ".00000009";
		document.getElementById('veg').style.opacity = ".00000009";
		document.getElementById('dessert').style.opacity = "1.0";
		document.getElementById('bev').style.opacity = ".00000009";
	};
}

//update pizza objects to menu page;
function pizza() {
	for (var i = 0; i < com.dawgpizza.menu.pizzas.length; i++) {
		//http://jsperf.com/html-insert-vs-createelement -> for results
	    var pizza = com.dawgpizza.menu.pizzas[i];
	    var name = '<li class="name"><strong>' + pizza.name + '</strong></li>';
	    name += '<li>' + pizza.description;
	    name += '<strong> <br/>';
	    for (var j = 0; j < pizza.prices.length; j++) {
	    	name += "$" + pizza.prices[j] + " ";
	    }
	    name += '</strong></li>';
	   	if (pizza.vegetarian) {
	   		document.getElementById('veg').innerHTML += name;
	   	}
	   	else {
	   		document.getElementById('meat').innerHTML += name;
	   	}
	} 
}

//update misc. beverages / desserts to page;
function misc(to, obj) {
	var ins = "";
	for (var i = 0; i < obj.length; i++) {
		ins += '<li>';
	    var item = obj[i];
	    ins += item.name + "(<strong>$"+ item.price + '</strong>) </li>';
	} 
	document.getElementById(to).innerHTML += ins;	
}