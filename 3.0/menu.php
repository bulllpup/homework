<?php include("top.html"); ?>
      <div id="index">
    <img class="outline" id="order_img" alt="Order at Your Convenience" src="pizza_files/pizza-order.jpg" />

    <h2 id="adj">All our pizzas are made with hand-thrown thin crust, baked in real wood ovens.
        Choose from one of these sizes:<br/>
       Small (12"), Medium (14"), Large (17")
    </h2>

    <p id="spec">All pizzas can be customized--add a few ingredients, or take a few
    off--for an extra $2.</p>

    <div itemprop="menu" id="options">
      <ul id="choose">
          <li id="piz"> Pizzas </li>
          <li id="drk"> Drinks </li>
          <li id="des"> Dessert </li>
      </ul>
      <ul class="pizza_pies" id="meat">
          <li class="name"><h2>Meat Pies</h2></li>
      </ul>

      <ul class="pizza_pies" id="veg">
           <li class="name"><h2>Vegetarian Pies</h2></li> 
      </ul>

      <ul class="disp" id="bev">
        <li class="name"><h2>Drinks</h2></li>
      </ul>

      <ul class="disp" id="dessert">
        <li class="name"><h2>Dessert</h2></li>
      </ul>
      
    </div><!-- Menu / Options -->
     <script src="pizza_files/menu.js" type="text/javascript"></script>
     <script src="pizza_files/site.js" type="text/javascript"></script>
    <?php include("bottom.html"); ?>
