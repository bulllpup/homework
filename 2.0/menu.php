<?php include("top.html"); ?>
      <div id="index">
    <img class="outline" id="order_img" alt="Order at Your Convenience" src="pizza_files/pizza-order.jpg" />

    <h2 id="adj">All our pizzas are made with hand-thrown thin crust, baked in real wood ovens.
        Choose from one of these sizes:<br/>
       Small (12"), Medium (14"), Large (17")
    </h2>

    <p id="spec">All pizzas can be customized--add a few ingredients, or take a few
    off--for an extra $2.</p>

    <div itemprop="menu" id="options">
      <ul id="meat">
        <li class="name">
          <h2>Meat Pies:</h2>
        </li>
        <li class="name"><strong>Classic Pepperoni</strong></li>
        <li>Pepperoni and Mozzarella on our Spicy Tomato Sauce.
        <strong>$10/$13/$16</strong></li>
        <li class="name"><strong>The Hawaiian</strong></li>
        <li>Canadian Bacon and Pineapple with Mozzarella on a rich Tomato Sauce.
        <strong>$12/$14/$17</strong></li>
        <li class="name"><strong>Duck, Duck, Goose</strong></li>
        <li>Roasted Duck and Goose with Bacon, Chestnuts on a rich Plum Sauce.
        <strong>$15/$17/$19</strong></li>
        <li class="name"><strong>The Bambi</strong></li>
        <li>Slow-cooked Venison with Red Cabbage on our famous Black Cherry Sauce.
        <strong>$15/$17/$19</strong></li>
        <li class="name"><strong>The Ultimate</strong></li>
        <li>Pepperoni, Bacon, Canadian Bacon, Chicken, Duck, Goose, and Ground Beef with
        Smoked Mozzarella on our Spicy Tomato Sauce. Add Venison or Elk for an extra $2!
        <strong>$15/$19/$23</strong></li>
      </ul>

      <ul id="veg">
        <li class="name">
          <h2>Vegetarian Pies:</h2>
        </li>
        <li class="name"><strong>Margherita</strong></li>
        <li>Mozzarella, Basil, Salt on an Olive Oil Base.
        <strong>$10/$13/$16</strong></li>
        <li class="name"><strong>Veggie Madness</strong></li>
        <li>Mushroom, Black Olive, Onions, Roasted Garlic, Squash and Roasted Eggplant on
        our Spicy Tomato Sauce. <strong>$11/$14/$17</strong></li>
        <li class="name"><strong>Forest Floor</strong></li>
        <li>Three kinds of mushrooms with Mozzarella on a herb Tomato Sauce.
        <strong>$11/$14/$17</strong></li>
        <li class="name"><strong>Mr Green</strong></li>
        <li>Roasted Tofu with Romano on a Basil Pesto sauce.
        <strong>$13/$16/$19</strong></li>
        <li class="name"><strong>Purple Monster</strong></li>
        <li>Roasted Eggplant and Cabbage stir fried in sesame oil on a rich Plum Sauce.
        <strong>$11/$14/$17</strong></li>
      </ul>

      <ul id="bev">
        <li class="name">
          <h2>Drinks:</h2>
        </li>
        <li>Coke, Diet Coke, Sprite, Root Beer, or Irn Bru by the can (<strong>$4</strong>)</li>
        <li>Rainier Beer by the can (<strong>$4</strong>)</li>
        <li>House Red Wine by the glass or bottle (<strong>$10/$40</strong>)</li>
      </ul>

      <ul id="dessert">
        <li class="name">
          <h2>Dessert:</h2>
        </li>
        <li>Chocolate Gelato (<strong>$8</strong>)</li>
        <li>Lemon Sorbet (<strong>$7</strong>)</li>
        <li>Ricotta Cheese Cake (<strong>$10</strong>)</li>
      </ul>
    </div><!-- Menu / Options -->
    <?php include("bottom.html"); ?>
