Website was built to replicate Pizza Website selling mainly to University Students in the Seattle Area. 

This website uses HTML5, CSS3, JavaScript, and PHP. Please be sure to commit any changes (when made).

Structure is as follows:

Most current page (generally):
http://students.washington.edu/connorco/info343/HW/pizza/
-> This page is set-up with Google analytics / Google indexing.

V 1.0: 
https://students.washington.edu/connorco/info343/HW/1.0/

v 2.0: 
https://students.washington.edu/connorco/info343/HW/2.0/

v 3.0: 
https://students.washington.edu/connorco/info343/HW/3.0/

v 4.0: 
https://students.washington.edu/connorco/info343/HW/4.0/

